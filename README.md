# ansible-demo-project




DEMO
====

This repo includes Ansible playbooks and the dependencies required to run them

SETUP
=====
- Clone this repo
- Create a new virtual environment (python3 -m venv tools)
- Install the contents of requirements.txt (pip install -r requirements.txt)
- Install the contents of requirements.yml (ansible-galaxy install -r requirements.yml)

PLAYBOOKS
=========

To run 'ansible-ping.yml', cd to the 'playbooks' directory.  then run
```
ansible-playbook ansible-ping.yml
```
which will run against localhost